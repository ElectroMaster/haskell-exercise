drop'::Int->[Int]->[Int]
drop' _ [] = []
drop' 0 xs = xs
drop' n (x:xs) = drop' (n-1) xs

takeRaw::Int->[Int]->[Int]->[Int]
takeRaw _ [] res = res
takeRaw 0 _ res = res
takeRaw m (x:xs) res = takeRaw (m-1) xs (res++[x])
take' :: Int->[Int]->[Int]
take' m xs = takeRaw m xs []

splitAt'::Int->[Int]->([Int],[Int])
splitAt' m xs
  | m>=length xs = (xs, [])
  | m<0 = ([], xs)
  | otherwise = (leftPart, rightPart)
    where leftPart = map (\(a,b)->b) (filter (\(a,b)->a<m) (zip [0..length xs] xs))
          rightPart = map (\(a,b)->b) (filter (\(a,b)->a>=m) (zip [0..length xs] xs))
main::IO()
main = do
  print(drop' 4 [4,5,6,7,8,9])
  print(drop' 10 [4,5,6,7,8,9])
  print(take' 2 [4,5,6,7,8,9])
  print(take' 10 [4,5,6,7,8,9])
  print(splitAt' 2 [4,5,6,7,8])
