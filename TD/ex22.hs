sum'::Integer->Integer
sum' 0 = 0
sum' x = x+sum' (x-1)
-- recursive terminale
sumT ::Integer->Integer
sumT' 0 res = res
sumT' x res = sumT' (x-1) res + x
sumT x = sumT' x 0


f::Integer->Integer->Integer->Integer
f _ 0 res = res
f num diviseur res
  | num `mod` diviseur == 0 = f num (diviseur-1) (res+diviseur)
  | otherwise = f num (diviseur-1) res
isParfait::Integer->Bool
isParfait x = let sumDiviseur = f x (x-1) 0 in x==sumDiviseur

main::IO()
main = do
  print(sum' 5)
  print(sumT 5)
  print(isParfait 6)
