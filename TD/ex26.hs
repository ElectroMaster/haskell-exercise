puissancef::(Int->Int)->Int->Int->Int
puissancef f 1 = f
puissancef f n = f . (puissancef f (n-1))

main::IO()
main = do
  print(puissancef (+1) 2 3)
