evalPoly::[Float]->Float->Float
evalPoly xs y= foldl (\acc (k,x) -> acc+x*y^k ) 0 (zip [0..length xs] xs)

main::IO()
main = do
  print(evalPoly [1,2,3] 4.0)
