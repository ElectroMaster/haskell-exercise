import Data.List

toMat [] m = []
toMat xs m = (take m xs) : toMat (drop m xs) m

produitMat::[[Int]]->[[Int]]->[[Int]]
produitMat xss yss
  |(row_xss /= col_xss) || (row_yss /= col_yss) || (row_xss /= row_yss) = error "error"
  |otherwise = toMat [sum1 | x<-xss, y<-transpose yss, let temp = zip x y, let sum1 = sum $ map (\(a,b)->a*b) temp] col_xss
  where row_xss = length xss
        col_xss = length (xss!!0)
        row_yss = length xss
        col_yss = length (yss!!0)
mat1 = [[1,2],[3,4]]
mat2 = [[5,6],[7,8]]

main::IO()
main = do
  print(produitMat mat1 mat2)
