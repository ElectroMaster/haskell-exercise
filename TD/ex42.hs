somePaire::[Int]->Int
somePaire = sum . filter (\x->x `mod` 2 == 0)

main::IO()
main = do
  print(somePaire [1,2,3,4])
