synracause::Integer->Integer->Integer
synracause x 0 = x
synracause x n
  |res `mod` 2==0 = res `div` 2
  |otherwise = 3*(synracause x (n-1))+1
  where res = synracause x (n-1)
main::IO()
main = do
  print(synracause 14 10)
