euclide::Integer->Integer->Integer
euclide a 0 = a
euclide a b
  | a<b = euclide b a
  | otherwise = euclide b (a `mod` b)

main::IO()
main = do
  print(euclide 45 15)
