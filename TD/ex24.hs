isMatched::[Char]->Bool
-- isMatched' ::[Char]->Integer->Integer->Bool
isMatched' [] left right = if left==right then True else False
isMatched' (x:xs) left right
  | x=='(' = isMatched' xs (left+1) right
  | x==')' = isMatched' xs left (right+1)
  | otherwise = isMatched' xs left right
isMatched xs = isMatched' xs 0 0
main::IO()
main = do
  print(isMatched "((())()())")
