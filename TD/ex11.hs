data Entier a = Un | Deux | Cinq | EntierSousForme a a

valeur::(Integral a)=>Entier a->a
valeur Un = 1
valeur Deux = 2
valeur Cinq = 5
valeur (EntierSousForme i j) = 3*i + 4*j

main::IO()
main = do
  print(valeur (EntierSousForme 3 9))
