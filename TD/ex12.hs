data Point = Pcartesien Float Float | Ppolaire Float Float

dOrigine::Point->Float
dOrigine (Pcartesien x y) = sqrt (x*x + y*y)
dOrigine (Ppolaire _ d) = d

sumdOrigine::[Point]->Float
sumdOrigine xs = sum [y | x<-xs, let y = dOrigine x]

main::IO()
main = do
  print(sumdOrigine [Pcartesien 0 1, Pcartesien 0 2])
