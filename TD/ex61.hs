argminf::(Int->Int)->Int->Int
argminf f x = minimum [y | y<-[0..x], f y >x]

main::IO()
main = print(argminf (+2) 10)
