rangeParis::[Int]->[Int]
rangeParis [] = []
rangeParis [x] = [x]
rangeParis (x:xs) = x:rangeParis (tail xs)

f1::[Int]->[(Int, Int)]
f1 xs = zip [0..length xs] xs
f2 xs = filter (\(a,b)->a `mod` 2 ==0) xs
f3 xs = map (\(a,b)->b) xs
-- f3::[Int]->[Int]
-- f3 xs = f2 (f1 xs)

main::IO()
main = do
  print(rangeParis [1..10])
  print(f1 [1..10])
  print(f3(f2 (f1 [1..10])))
