data Tree  = Empty | Node String Integer (Tree) (Tree) deriving(Show)

myTree = Node "+" 0 (Node "" 1 (Empty) (Empty)) (Node "*" 0 (Node "" 2 (Empty) (Empty)) (Node "" 3 (Empty) (Empty)))

evaluate::Tree->Integer
evaluate Empty = 0
evaluate (Node _ val Empty Empty) = val
evaluate (Node "+" _ tree1 tree2) = evaluate tree1 + evaluate tree2
evaluate (Node "*" _ tree1 tree2) = evaluate tree1 * evaluate tree2
main::IO()
main = do
  print(myTree)
  print(evaluate myTree)
