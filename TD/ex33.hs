
ord::Char->Int
ord alp = fst . head $ filter (\(a,b)->b==alp) (zip [0..25] ['a'..'z'])

chr::Int->Char
chr m
  | m>25 = chr (m-26)
  | otherwise = snd . head $ filter (\(a,b)->a==m) (zip [0..25] ['a'..'z'])

cesar m xs = map chr (map (\x->ord x + m) xs)
main::IO()
main = do
  print(ord 'a')
  print(chr 0)
  print(cesar 13 "pbva")
  print $ zip [0..30] ['a'..'z']
