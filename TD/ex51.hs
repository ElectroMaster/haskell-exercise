reverse'::(Num a)=>[a]->[a]
reverse' xs = foldl (\acc x-> x:acc) [] xs

main::IO()
main = do
  print(reverse' [1,2,3,4])
