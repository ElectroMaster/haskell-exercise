import Data.List
mat1 = [[1,2],[3,4]]
mat2 = [[5,6],[7,8]]

test xs ys= [sum1 | x<-xs, y<- transpose ys, let temp = zip x y, let sum1 = sum $ map (\(a,b)->a*b) temp]
mat3 = test mat1 mat2
toMat [] m = []
toMat xs m = (take m xs) : toMat (drop m xs) m
main::IO()
main = do
  print(test mat1 mat2)
  print(transpose mat2)
  print(length (mat1!!0))
  print(mat3)
  print(take 2 mat3)
  print(drop 2 mat3)
  print(toMat mat3 2)
