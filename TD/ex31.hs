map2D f xss = map (\xs -> map f xs) xss

my = [[1,2,3],[4,5,6],[7,8,9]]
main::IO()
main = do
  print(map2D (+1) my)
