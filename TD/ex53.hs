map'::(a->b)->[a]->[b]
map' f [] = []
map' f xs = foldl (\acc x -> acc ++ [f x]) [] xs
main::IO()
main = do
  print(map' (+1) [1,2,3,4])
