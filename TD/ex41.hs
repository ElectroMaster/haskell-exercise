import Data.List
import Data.Ord
my = [4,3,2,3,7,5,6]

toList [x] = [[x]]
toList all@(x:xs) = [all] ++ (toList xs)

isSorted::(Ord a)=>[a]->Bool
isSorted [] = True
isSorted [x] = True
isSorted (x:y:xs) = x<=y && isSorted (y:xs)

maxSubList::Ord a=>[a]->[a]
maxSubList = maximumBy (comparing length) . map nub . filter isSorted . subsequences
test = map nub . filter isSorted . subsequences

maxSub::[Int]->Int
maxSub xs = length $ maxSubList xs

main::IO()
main = do
  print(toList my)
  print(test my)
  print(maxSubList my)
