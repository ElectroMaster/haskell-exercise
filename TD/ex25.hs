minPath::[[Int]]->Int
minPath xss = let
                x = head $ head xss
                yss = tail xss
                zss = map tail xss
              in
                if null yss
                  then if null $ head zss
                    then x
                    else x + minPath zss
                else if null $ head zss
                  then x + minPath yss
                  else x + min (minPath yss) (minPath zss)
matrice = [[1, 12, 57, 74], [32, 42, 72, 3], [1, 55, 45, 2]]

main::IO()
main = do
  print(minPath matrice)
  print(map tail matrice)
