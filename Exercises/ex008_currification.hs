compareWithHundred :: (Num a, Ord a) => a -> Ordering
compareWithHundred = compare 100

divideByTen::(Floating a)=>a->a
divideByTen = (/10)

applyTwice:: (a->a) ->a ->a
applyTwice f x = f (f x)

quicksort::(Ord a)=>[a]->[a]
quicksort [] = []
quicksort (x:xs) =
  let smallerSorted = quicksort (filter (<=x) xs)
      biggerSorted = quicksort (filter (>=x) xs)
  in smallerSorted ++ [x] ++ biggerSorted

largestDivisible::(Integral a)=>a
largestDivisible = head (filter p [100000, 99999..])
                    where p x = x `mod` 3829 == 0
prime::(Integral a)=>a->Bool
prime x
  | x<=1 = False
  |otherwise = let len = length (filter p [1..x])
          in  len==2
          where p y = x `mod` y == 0

numGrandThanFive::(Integral a)=>[a]->[a]
numGrandThanFive xs = filter (\x -> x>5) xs

sum' :: (Num a)=>[a]->a
sum' xs = foldl (\acc x->acc+x) 0 xs

sumCurried :: (Num a)=>[a]->a
sumCurried = foldl (+) 0

elem' :: (Eq a)=>a->[a]->Bool
elem' x ys = foldl (\acc y ->if x == y then True else acc) False ys

map' :: (a->b)->[a]->[b]
map' f xs = foldr (\x acc-> f x :acc) [] xs

main::IO()
main = do
  print(compareWithHundred 101)
  print(divideByTen 9)
  print(applyTwice (+3) 10)
  print(map (+3) [1,2,3,4])
  print(filter (>3) [1,2,3,4,5])
  print(quicksort [12,4, 69,34, 1])
  print(largestDivisible)
  print(prime 3)
  print(prime 0)
  print(prime 1)
  print(filter prime [1..100])
  print(filter odd [1..50])
  print(filter even [1..50])
  print(numGrandThanFive [2,4,5,6,7])
  print(sum' [1,2,3,4])
  print(sumCurried [1,2,3,4])
  print(elem' 5 [2,3,4,5])
  print(map' (+5) [1,2,3])
