maximum' :: (Ord a)=>[a]->a
maximum' [] = error "empty list"
maximum' [x] = x
maximum' (x:xs)
  | x>maxTail = x
  | otherwise = maxTail
  where maxTail = maximum' xs

replicate' :: (Num i, Ord i)=>i->a->[a]
replicate' n x
  | n<=0 = []
  | otherwise = x: replicate' (n-1) x

take' :: (Num i, Ord i) => i -> [a] -> [a]
take' n _
  | n<=0 = []
take' _ []     = []
take' n (x:xs) = x : take' (n-1) xs

zip' :: [a]->[b]->[(a, b)]
zip' _ [] = []
zip' [] _ = []
zip' (x:xs) (y:ys) = (x,y):zip' xs ys


main::IO()
main = do
  print(maximum' [4,5,7,6])
  print(replicate' 3 5)
  print(take' 2 [1,2,3,4])
  print(zip' [1,2,3] ['a', 'b', 'c', 'd'])
