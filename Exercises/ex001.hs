doubleMe x = x + x
doubleUs x y = x*2 + y*2
doubleUs_f x y = doubleMe x + doubleMe y
doubleSmallNumber x = if x>100
                        then x
                        else x*2
doubleSmallNumber' x = (if x>100 then x else x*2) + 1
distance (x, y) (x1, y1) = sqrt ((x1-x)^2 + (y1-y)^2)
my = [x*y | x<- [1..5], y<-[6..10], x*y>30]
main::IO()
main = do
  print(doubleMe 5)
  print(doubleUs 5 4)
  print(doubleUs_f 5 4)
  print(doubleSmallNumber 101)
  print(doubleSmallNumber 10)
  print(doubleSmallNumber' 101)
  print(doubleSmallNumber' 10)
  print(reverse [1, 2, 3, 4])
  print(odd 1)
  print(odd 4)
  print(my)
  print(distance (0,3) (4,0))
