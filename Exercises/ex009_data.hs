data Shape = Point Float Float | Circle Float Float Float | Rect Float Float Float Float deriving (Show)

data Point1 = Point1 Float Float deriving (Show)
data Shape1 = Circle1 Point1 Float | Rect1 Point1 Point1 deriving (Show)

surface::Shape->Float
surface (Point _ _) = 0
surface (Circle _ _ r) = pi*r*r
surface (Rect x1 y1 x2 y2) = (abs $ x2-x1)*(abs $ y2-y1)

data Person = Person {firstName::String , lastName:: String, phone::Integer} deriving(Show)
guy = Person "Buddy" "Champion" 123456

data Car a b c = Car { company :: a
                     , model :: b
                     , year :: c
                     } deriving (Show)
tellCar:: (Show a)=> Car String String a -> String
tellCar (Car {company=c, model=m, year=y}) = "This " ++ c ++ " " ++ m ++ " was made in " ++ show y

data Vector a  = Vector a a a deriving(Show)
vplus :: (Num t) => Vector t -> Vector t -> Vector t
(Vector i j k) `vplus` (Vector l m n) = Vector (i+l) (j+m) (k+n)
main::IO()
main = do
  print(surface (Circle 1 3 4))
  print(surface (Rect 1 3 2 4))
  print(surface (Point 1 3))
  print(guy)
  print(firstName guy)
  print(tellCar (Car "Ford" "Mustang" "nineteen sixty seven")  )
  print(tellCar (Car "Ford" "Mustang" 1967)  )
  print(Vector 1 3 5 `vplus` Vector 2 4 6)
