lucky::(Integral a)=> a->String
lucky 1 = "hello"
lucky 2 = "hello world"
lucky x = "not in 1 or 2"

factorial :: (Integral a)=> a->a
factorial 0 = 1
factorial x = x*factorial (x-1)
addVectors::(Num a)=> (a, a)->(a, a)->(a, a)
addVectors (x, y) (x1, y1) = (x+x1, y+y1)

head' :: [a]->a
head' [] = error "Can't call head on an empty list, dummy"
head' (x:_) = x

length' :: (Num b)=> [a]->b
length' [] = 0
length' (_:xs) = 1 + length' xs

sum' :: (Num a)=>[a]->a
sum' [] = 0
sum' (x:xs) = x + sum' xs

tellMeTwoNum :: (Ord a) => a->a->String
tellMeTwoNum x y
  | x<y = "x inferieur a y"
  | x>y = "x superieur a y"
  | otherwise = "x egale a y"

calcBmis :: (RealFloat a) => [(a, a)] -> [a]
calcBmis xs = [bmi | (w, h) <- xs, let bmi = w / h ^ 2, bmi >= 25.0]

describleList :: [a]->String
describleList xs = "The list is " ++ case xs of [] -> "empty"
                                                [x] -> "a single list"
                                                xs -> "a longer list"
main::IO()
main = do
  print(lucky 1)
  print(lucky 4)
  print(factorial 3)
  print(addVectors (4, 5) (6,7))
  print(head' [1,2,3,4])
  print(length' ["1", "b", "c"])
  print(sum' [1, 2, 3])
  print(tellMeTwoNum 5 6)
  print(describleList "Wow")
