quicksort::(Ord a)=>[a]->[a]
quicksort [] = []
quicksort (x:xs) =
  let smallerSorted = quicksort [a | a<-xs, a<=x]
      biggerSorted = quicksort [a | a<-xs, a>=x]
  in smallerSorted ++ [x] ++ biggerSorted

main::IO()
main = do
  print(quicksort [5,2,6,8,1])
