removeNonUppercase::[Char]->[Char]
removeNonUppercase xs = [x | x<-xs, x `elem` ['A'..'Z']]

addThree :: Int->Int->Int->Int
addThree a b c = a+b+c

main::IO()
main = do
  print(removeNonUppercase "hello WORLD")
  print(addThree 1 3 5)
