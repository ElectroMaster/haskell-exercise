reverse' :: [a]->[a]
reverse' [] = []
reverse' (x:xs) = reverse' xs ++ [x]

reverseNum :: (Integral a)=>a->a
reverseNum' 0 y = y
reverseNum' x y = reverseNum' (x `div` 10) (10*y + (x `mod`10))
reverseNum x = reverseNum' x 0

main::IO()
main = do
  print(reverse' [1,2,3,4])
  print(reverseNum 123)
